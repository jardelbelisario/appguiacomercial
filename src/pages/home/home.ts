import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, PopoverController, NavParams, ViewController, MenuController } from 'ionic-angular';
import { AppHttpProvider } from '../../providers/app-http/app-http';
import * as config from '../../config';
import { SingleCompanyPage } from './../single-company/single-company';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  companiesDestaque: any = [];
  companies: any = [];
  companiesParams: any = [];
  url_img_site_guia = config.URL_IMG_SITE_GUIA;
  morePagesAvailable: boolean = true;
  tamnhoArray: any;
  somaPage: any;

  constructor(
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public appHttpProvider: AppHttpProvider,
    public alertCtrl: AlertController,
    public popoverCtrl: PopoverController
  ) {}


  // ionViewWillEnter(){
  ionViewDidLoad() {

    this.companiesParams = this.navParams.get('companies') || this.ionViewDidLoad2();
    this.companies = this.companiesParams;
    console.log("Por Parametro: ", this.companies)

    // this.menuCtrl.enable(true);
    // this.menuCtrl.toggle();
  }

  ionViewDidLoad2(){

    // let loader = this.loadingCtrl.create();
    // loader.setContent("Carregando...");
    // // loader.present();

    return this.appHttpProvider.getListData('companies?order=company_featured,desc',1)
    .subscribe(result => {
      // loader.dismiss();
      this.companies = result;
      console.log(this.companies);
    }, err => {
      // loader.dismiss();
      console.log("Erro: " + err);
    })
  }

  getSingleCompanyPage(objeto){
    this.navCtrl.push(SingleCompanyPage, { 
      objeto: objeto
    })
  }

  // ionViewDidLoad() {
  //   this.appHttpProvider
  //     .getListDataShopping(
  //       "https://sandbox-api.lomadee.com/v2/152522851422067717342/offer/_bestsellers?sourceId=35966623"
  //     )
  //     .subscribe(result => {
  //       // this.dados = result['offers'];
  //       this.dados = result;
  //     });
  // }

  ionViewDidLoadCompanies() {
    this.appHttpProvider
    .getListData("companies?where[company_featured]=1", 1)
      .subscribe(
        res => {
          this.companies = res["data"];
          console.log(this.companies);
        },
        err => {
          console.log("Erro: " + err);
        }
      );
  }

  doInfinite(infiniteScroll) {
    //A função Math.ceil(x) retorna o maior número inteiro maior ou igual a "x".

    this.tamnhoArray =  this.companies["data"].length;

    console.log("Tamanho Array: " + this.tamnhoArray)

    
    let page = Math.ceil(this.companies["data"].length / 20) + 1;
    this.somaPage = this.tamnhoArray/(page - 1);
    console.log("Som page: " + this.somaPage )

    console.log("page: " + page);
    let loading = true;

    this.appHttpProvider
    .getListData("companies?order=company_featured,desc", page)
      .subscribe(
        result => {
          for (let post of result["data"]) {
            if (!loading) {
              infiniteScroll.complete();
            }
            this.companies["data"].push(post);
            loading = false;
          }
          if (page == this.companies.last_page) {
            infiniteScroll.complete();
            loading = false;
            this.morePagesAvailable = false;
          }
          // this.ionViewDidLoadCompanies();
        },
        error => {
          console.log("Erro infinite Scroll" + error);
          this.morePagesAvailable = false;
        }
      );
  }

  // presentPopover(myEvent) {

  //   let popover = this.popoverCtrl.create(FilterPopoverPage, null, null);
  //   popover.present({
  //     ev: myEvent
  //   });
    

  //   // this.viewCtrl.dismiss();
  // }

  showAlertFunction() {
    let alert = this.alertCtrl.create({
      title: 'New Friend!',
      subTitle: 'Your friend, Obi wan Kenobi, just accepted your friend request!',
      buttons: ['OK']
    });
    alert.present();
  }

}
