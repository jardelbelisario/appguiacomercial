import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
//import { MapaCompanyComponent } from './../../components/mapa-company/mapa-company';

// import {
//   GoogleMaps,
//   GoogleMap,
//   GoogleMapsEvent,
//   GoogleMapOptions,
//   CameraPosition,
//   MarkerOptions,
//   Marker
// } from "@ionic-native/google-maps";

/**
 * Generated class for the SinglecompanyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-singlecompany",
  templateUrl: "single-company.html"
})
export class SingleCompanyPage {
  objeto: any;
  conteudoInnerHTML: any;
  imgCapaCompany: any = '';
  // map: GoogleMap;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.objeto = this.navParams.get("objeto");

    this.conteudoInnerHTML = this.objeto["company_history"];
    // console.log("Conteudo InnerHTML: " + this.conteudoInnerHTML);

    console.log("capa Objeto: ", this.objeto["company_capa"])

    if(this.objeto['company_capa'] != null){
      this.imgCapaCompany = `linear-gradient( 
        rgba(0, 0, 0, 0.45), rgba(0, 0, 0, 0.45)), 
      url(https://www.guiaimperatriz.com.br/tim.php?src=uploads/${this.objeto["company_capa"]}&w=500&h=200)`;
    }else{ 
       this.imgCapaCompany = `linear-gradient( 
         rgba(0, 0, 0, 0.45),  rgba(0, 0, 0, 0.45)), 
       url(assets/images/img_padrao.jpg)`;
    }

    console.log("Capa: ", this.imgCapaCompany);

    console.log("Objeto Company: ", this.objeto);
  }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad SinglecompanyPage');
  // }

  ionViewDidLoad() {
    this.loadMap();
  }

  loadMap() {

    // let map: GoogleMap = GoogleMaps.create("map_canvas1");
    // map.one(GoogleMapsEvent.MAP_READY).then(() => {
    //   console.log("--> map_canvas1 : ready.");
    // });


    
    // let mapOptions: GoogleMapOptions = {
    //   camera: {
    //      target: {
    //        lat: 43.0741904,
    //        lng: -89.3809802
    //      },
    //      zoom: 18,
    //      tilt: 30
    //    }
    // };

    // this.map = GoogleMaps.create('map_canvas');
    // this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
    //   console.log("--> map_canvas3 : ready.");
    // });

    // let marker: Marker = this.map.addMarkerSync({
    //   title: 'Ionic',
    //   icon: 'blue',
    //   animation: 'DROP',
    //   position: {
    //     lat: 43.0741904,
    //     lng: -89.3809802
    //   }
    // });
    // marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
    //   alert('clicked');
    // });
  }

  getBotao() {
    alert("Clicou no botão");
  }
}
