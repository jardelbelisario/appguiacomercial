import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { AppHttpProvider } from '../../providers/app-http/app-http';
import * as config from '../../config';
/**
 * Generated class for the CategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class CategoriesPage {
  categories: any = [];
  url_svg_site_guia = config.URL_SVG_SITE_GUIA;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,  
    public appHttpProvider: AppHttpProvider) {
  }

  

  ionViewDidLoad(){

    let loader = this.loadingCtrl.create();
    loader.setContent("Carregando...");
    loader.present();

    this.appHttpProvider.getListData('categories?order=category_id,asc', 1)
    .subscribe(result => {
      loader.dismiss();
      this.categories = result;
      console.log(this.categories);
    }, err =>{
      console.log('Erro categories: ' + err )
    })
  }

  getPostSingle(objeto){

  }

  // ionViewDidLoad(){
  //   this.appHttpProvider.getListData('companies?order=company_featured,desc',1)
  //   .subscribe(res => {
  //     this.companies = res;
  //     console.log(this.companies);
  //   })
  // }

}
