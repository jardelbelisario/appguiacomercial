import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostsSinglePage } from './posts-single';

@NgModule({
  declarations: [
    PostsSinglePage,
  ],
  imports: [
    IonicPageModule.forChild(PostsSinglePage),
  ],
})
export class PostsSinglePageModule {}
