import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, LoadingController } from "ionic-angular";
import { AppHttpProvider } from "../../providers/app-http/app-http";
import * as config from "../../config";
import { PostsSinglePage } from "../posts-single/posts-single";

/**
 * Generated class for the PostsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-posts",
  templateUrl: "posts.html"
})
export class PostsPage {
  posts: any = [];
  url_img_site_guia = config.URL_IMG_SITE_GUIA;


  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public appHttpProvider: AppHttpProvider
  ) {

    let loader = this.loadingCtrl.create();
    loader.setContent("Carregando...");
    loader.present();

    this.appHttpProvider
      .getListData("posts?order=post_date,desc", 1)
      .subscribe(data => {
        loader.dismiss();
        this.posts = data;
        console.log(this.posts);
      });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad PostsPage");
  }

  getPostSingle(objeto){
    this.navCtrl.push(PostsSinglePage, {
      objeto: objeto
    })
  }

}
