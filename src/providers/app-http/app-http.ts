import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as config from '../../config';
/*
  Generated class for the AppHttpProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AppHttpProvider {

  constructor(public http: HttpClient) {
   

  }

  getRecentPosts(page: number = 1 ){
    return this.http.get(config.URL_API_REST + 'posts?page='+ page)
  }

  getRecentPostsNutri(page: number = 1 ){
    return this.http.get(config.URL_API_REST + 'posts?page='+ page)
  }


  getListData(parametro, page:number = 1){
    return this.http.get(config.URL_API_REST + parametro + '&page='+ page)
  }

  getListDataShopping(parametro){
    return this.http.get(parametro)
  }



  getListData2(parametro){
    return this.http.get(config.URL_API_REST + parametro)
  }


}
