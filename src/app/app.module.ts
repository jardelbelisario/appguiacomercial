import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpModule } from "@angular/http";
import { HttpClientModule } from "@angular/common/http";
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { ParallaxHeaderDirective } from '../directives/parallax-header/parallax-header';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { CategoriesPage } from "../pages/categories/categories";
import { CompanyPage } from "../pages/company/company";
import { PostsPage } from "../pages/posts/posts";
import { PostsSinglePage } from "../pages/posts-single/posts-single";
import { ShoppingPage } from "../pages/shopping/shopping";
import { SingleCompanyPage } from "../pages/single-company/single-company";




import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AppHttpProvider } from '../providers/app-http/app-http';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    CategoriesPage,
    CompanyPage,
    PostsPage,
    PostsSinglePage,
    ShoppingPage,
    ParallaxHeaderDirective,
    SingleCompanyPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    CategoriesPage,
    CompanyPage,
    PostsPage,
    PostsSinglePage,
    ShoppingPage,
    SingleCompanyPage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AppHttpProvider
  ]
})
export class AppModule {}
